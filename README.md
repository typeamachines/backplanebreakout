Etray distribution breakout PCB 
-------------------------------
Designed for Series 1 2014 Model, using geda:gaf 

Work on this repo using a centralized git workflow

This small pcb connects signals from RAMPS/RUMBA/RAMBO type 3D printer controllers and sends them out to gantry pcb and carriage pcb via 6 pin 24AWG and 26 conductor high density ribbon cable.

Refer to semver.org for versioning information. As far as I know, symantic
versioning hasn't been used for HW before, workflows will be added as they become clear in the master overview of the systems integrated with this board. 
