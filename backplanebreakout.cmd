# Pin name action command file

# Start of element CONN29
ChangePinName(CONN29, 2, 2)
ChangePinName(CONN29, 1, 1)

# Start of element CONN28
ChangePinName(CONN28, 11, 11)
ChangePinName(CONN28, 9, 9)
ChangePinName(CONN28, 12, 12)
ChangePinName(CONN28, 10, 10)
ChangePinName(CONN28, 7, 7)
ChangePinName(CONN28, 5, 5)
ChangePinName(CONN28, 3, 3)
ChangePinName(CONN28, 1, 1)
ChangePinName(CONN28, 8, 8)
ChangePinName(CONN28, 6, 6)
ChangePinName(CONN28, 4, 4)
ChangePinName(CONN28, 2, 2)

# Start of element CONN24
ChangePinName(CONN24, 4, 4)
ChangePinName(CONN24, 3, 3)
ChangePinName(CONN24, 2, 2)
ChangePinName(CONN24, 1, 1)

# Start of element CONN21
ChangePinName(CONN21, 4, 4)
ChangePinName(CONN21, 3, 3)
ChangePinName(CONN21, 2, 2)
ChangePinName(CONN21, 1, 1)

# Start of element CONN20
ChangePinName(CONN20, 6, gnd)
ChangePinName(CONN20, 5, Vin_Vdc)
ChangePinName(CONN20, 4, HeatRtn)
ChangePinName(CONN20, 3, Heat1)
ChangePinName(CONN20, 2, HeatRtn)
ChangePinName(CONN20, 1, Heat0)

# Start of element CONN19
ChangePinName(CONN19, 26, 5v_vcc)
ChangePinName(CONN19, 25, aux0-b)
ChangePinName(CONN19, 24, aux0-a)
ChangePinName(CONN19, 23, therm0)
ChangePinName(CONN19, 22, fan0)
ChangePinName(CONN19, 21, gnd)
ChangePinName(CONN19, 20, fan0_1)
ChangePinName(CONN19, 19, fan1_1)
ChangePinName(CONN19, 18, therm1)
ChangePinName(CONN19, 17, fan1)
ChangePinName(CONN19, 16, x_endstop)
ChangePinName(CONN19, 15, gnd)
ChangePinName(CONN19, 14, XM_1B)
ChangePinName(CONN19, 13, XM_1A)
ChangePinName(CONN19, 12, XM_2A)
ChangePinName(CONN19, 11, XM_2B)
ChangePinName(CONN19, 10, gnd)
ChangePinName(CONN19, 9, E1M_1B)
ChangePinName(CONN19, 8, E1M_1A)
ChangePinName(CONN19, 7, E1M_2A)
ChangePinName(CONN19, 6, E1M_2B)
ChangePinName(CONN19, 5, gnd)
ChangePinName(CONN19, 4, E0M_1B)
ChangePinName(CONN19, 3, E0M_1A)
ChangePinName(CONN19, 2, E0M_2A)
ChangePinName(CONN19, 1, E0M_2B)

# Start of element CONN14
ChangePinName(CONN14, 2, 2)
ChangePinName(CONN14, 1, 1)

# Start of element CONN18
ChangePinName(CONN18, 4, 4)
ChangePinName(CONN18, 3, 3)
ChangePinName(CONN18, 2, 2)
ChangePinName(CONN18, 1, 1)

# Start of element CONN17
ChangePinName(CONN17, 2, 2)
ChangePinName(CONN17, 1, 1)

# Start of element CONN16
ChangePinName(CONN16, 2, 2)
ChangePinName(CONN16, 1, 1)

# Start of element CONN15
ChangePinName(CONN15, 2, 2)
ChangePinName(CONN15, 1, 1)

# Start of element CONN3
ChangePinName(CONN3, 4, 4)
ChangePinName(CONN3, 3, 3)
ChangePinName(CONN3, 2, 2)
ChangePinName(CONN3, 1, 1)

# Start of element CONN2
ChangePinName(CONN2, 4, 4)
ChangePinName(CONN2, 3, 3)
ChangePinName(CONN2, 2, 2)
ChangePinName(CONN2, 1, 1)

# Start of element CONN1
ChangePinName(CONN1, 4, 4)
ChangePinName(CONN1, 3, 3)
ChangePinName(CONN1, 2, 2)
ChangePinName(CONN1, 1, 1)
